<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
 private $table = "products";

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seed();
    }

    /**
     * Main seed
     *
     * @return void
     */
    public function seed()
    {
        DB::table($this->table)->delete();
        $data = $this->getData();
        DB::table($this->table)->insert($data);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        return [
            [
                "id" => 1,
                "name" => "Pen",
                "client_id"=>1,
            ],
            [
                "id" => 2,
                "name" => "Bag",
                "client_id"=>2,
            ],
        ];
    }
}
