<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    private $table = "users";

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seed();
    }

    /**
     * Main seed
     *
     * @return void
     */
    public function seed()
    {
        DB::table($this->table)->delete();
        $data = $this->getData();
        DB::table($this->table)->insert($data);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        return [
            [
                "id" => 1,
                "name" => "Pokahontas",
                "email"=>"andsd@scsds.com",
                "password"=>"assd",
            ],
            [
                "id" => 2,
                "name" => "Jonny Depp",
                "email"=>"andsd2@scsds.com",
                "password"=>"assd",
            ],
        ];
    }

}
