<?php

use Illuminate\Database\Seeder;

class ClientsTableSeeder extends Seeder
{
 private $table = "clients";

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seed();
    }

    /**
     * Main seed
     *
     * @return void
     */
    public function seed()
    {
        DB::table($this->table)->delete();
        $data = $this->getData();
        DB::table($this->table)->insert($data);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        return [
            [
                "id" => 1,
                "name" => "Armadillo",
                "email"=>"rthjk@scsds.com",
                "password"=>"sdvdfgr",
                "type"=>"admin",
            ],
            [
                "id" => 2,
                "name" => "Pinocchio",
                "email"=>"zsdf@scsds.com",
                "password"=>"drfsdgrt",
                "type"=>"user",
            ],
        ];
    }
}
