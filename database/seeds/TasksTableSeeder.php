<?php

use Illuminate\Database\Seeder;

class TasksTableSeeder extends Seeder
{

    private $table = "tasks";

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seed();
    }

    /**
     * Main seed
     *
     * @return void
     */
    public function seed()
    {
        DB::table($this->table)->delete();
        $data = $this->getData();
        DB::table($this->table)->insert($data);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        return [
            [
                "id" => 1,
                "name" => "Task 1",
                "user_id"=>1,
            ],
            [
                "id" => 2,
                "name" => "Task 2",
                "user_id"=>2,
            ],
        ];
    }

}
