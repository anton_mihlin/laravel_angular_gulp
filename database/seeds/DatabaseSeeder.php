<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       // Users
       $this->call(UsersTableSeeder::class);
       $this->call(TasksTableSeeder::class);
       
       // Clients
       $this->call(ClientsTableSeeder::class);
       $this->call(ProductsTableSeeder::class);
    }
}
