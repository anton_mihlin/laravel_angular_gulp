var gulp = require('gulp');

var elixir = require('laravel-elixir');

var del = require('del'); // execute: $ npm install --save-dev del

var BrowserSync = require('laravel-elixir-browsersync');

require('laravel-elixir-jade');

require('laravel-elixir-clean');

require('elixir-jshint');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

var path = {
};





gulp.task("remove", function () {
    del(['public/vendor/*','public/css/*','public/js/*','public/build/*']); 
});

elixir.config.sourcemaps = false;

elixir(function (mix) {
    //BrowserSync.init();
    mix
            /* ------------------------------
             * jshint
             * Browser sync
             * Create path var
             * Bower packs
             */
            .task('remove')
            // jQuery
            .scripts('js/jquery.js', 'public/vendor/jquery/js/jquery.js', 'resources/assets/vendor/jquery')
            //Bootstrap
            .styles('css/bootstrap.css', 'public/vendor/bootstrap/css/bootstrap.css', 'resources/assets/vendor/bootstrap')
            .styles('css/bootstrap-theme.css', 'public/vendor/bootstrap/css/bootstrap-theme.css', 'resources/assets/vendor/bootstrap')
            .styles('css/print.css', 'public/vendor/bootstrap/css/print.css', 'resources/assets/vendor/bootstrap')
            .scripts('js/bootstrap.js', 'public/vendor/bootstrap/js/bootstrap.js', 'resources/assets/vendor/bootstrap')
            .copy('resources/assets/vendor/bootstrap/fonts/**', 'public/vendor/bootstrap/fonts')
            //Copy TXT files
            //.copy('resources/assets/txt/**', 'public/txt')
            //SASS
            .sass('example.scss', 'public/css/example.css')

            .sass('antonStyle.scss', 'public/css/antonStyle.css')
            //Js
            .jshint(['js/**/*.js', '!public/js/*.js'])

            .scripts('layout.js', 'public/js/layout.js')

            .scripts('antonScript.js', 'public/js/antonScript.js')

            //JADE
            .jade({
                search: '*.jade',
                dest: '/views/pages/clients'
            })

            //VERSION
            .version([
                'css/example.css',
                'js/layout.js',
                'js/antonScript.js',
                'css/antonStyle.css'
            ])
            .browserSync({
                proxy: "http://localhost/projects/PhpProject1/public/clients"
            })
            ;
});
