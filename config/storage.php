<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Storage Paths Data
    |--------------------------------------------------------------------------
    */

    'basePath' => '\\app',
    'tempPath' => '\\files\\temp\\',
    'textPath' => '\\files\\txt\\',
    'logsPath' => '\\files\\logs\\',

];
