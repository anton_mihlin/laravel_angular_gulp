@if (count($errors) > 0)
    <div id="errors">
        <div class="container">
            @foreach($errors->all() as $error)
                <div class="alert alert-danger alert-dismissable" role="alert">
                    <a class="close" data-dismiss="alert">&times;</a>
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    {{ $error }}
                </div>
            @endforeach
        </div>
    </div>
@endif