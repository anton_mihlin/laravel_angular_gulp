@extends('pages.mainView')

@section('form')
<form method="post" action="{{ route($route, 2) }}">
    
    <p>{{ Session::get('formResponse') }}</p>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="submit">            
</form>
<h4>{{ trans('food-names.cucumber') }}</h4>
@endsection