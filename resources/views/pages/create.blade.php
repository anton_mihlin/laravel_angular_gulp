<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <style>
            td{
                 border:1px solid #333;
                 padding:10px;
            }            
        </style>
    </head>
    <body>
        @include('pages.partials.errors')
        
        <form action="{{route('treeApp.store')}}" method="post">
            <input style='color:red;' type="hidden" name="_token" value="{{ csrf_token() }}">
            <p>
                <label>Name</label>
                <input type="text" name="name" >                
            </p>
            <p>
                <label>User</label>
                <select name="user">
                    @foreach( $users as $user)
                    <option value="{{ $user['id'] }}">{{ $user['name'] }}</option>
                    @endforeach
                </select>
            </p>
            <input type="submit">
        </form>
        
    </body>
</html>