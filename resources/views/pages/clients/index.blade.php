<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <link href="{{ asset('vendor/bootstrap/css/bootstrap.css') }}" rel="stylesheet" />
        <link href="{{ asset('vendor/bootstrap/css/bootstrap-theme.css') }}" rel="stylesheet" />
        <link href="{{ asset('vendor/bootstrap/css/print.css') }}" rel="stylesheet" />
        <link href="{{ asset(elixir('css/example.css')) }}" rel="stylesheet" />
        <link href="{{ asset(elixir('css/antonStyle.css')) }}" rel="stylesheet" />

    </head>
    <body>
        <form action="{{ route('clients.index') }}" method="post">
            <label>Check to redirect</label>
            <input style='color:red;' type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="checkbox" name="redirect-checkbox">
            <input type="submit" value="Submit">
        </form>


        @if (Session::has('myMessage'))
        <h2>{{ Session::get('myMessage') }}</h2>
        @endif

        <table>
            <tr>
                <td>Id</td>
                <td>Name</td>
                <td>Client id</td>
                <td>Full name</td>
                <td>Action</td>
            </tr>
            @foreach( $models as $item)
            <tr>
                <td>{{$item['id']}}</td>
                <td>{{CleanString::removeSpecialChar($item['name'])}}</td>
                <td>{{$item['client_id']}}</td>
                <td>{{ $item['full_name'] }}</td>
                <td>
                    <a href="{{ route('clients.edit', $item['id']) }}">Edit</a>
                    <a href="{{ route('clients.delete', $item['id']) }}">Delete</a>
                </td>
            </tr>
            @endforeach            
        </table>
        <div class="mail-icon-wrapper">
            <span class="mail-icon glyphicon glyphicon-envelope"></span>
        </div>
        
        
        <h2><a class="btn btn-primary" href="{{route('clients.create')}}">Create a new product</a></h2>
        
        <div class="jumbotron">
            <div class="change-color btn btn-default">Change Color</div>
        </div>
        
        
        @include('pages/clients/testJade')
        
        
        <script type="text/javascript" src="{{ asset('vendor/jquery/js/jquery.js') }}"></script>
        <script type="text/javascript" src="{{ asset('vendor/bootstrap/js/bootstrap.js') }}"></script>
        <script type="text/javascript" src="{{ asset(elixir('js/layout.js')) }}"></script>
        <script type="text/javascript" src="{{ asset(elixir('js/antonScript.js')) }}"></script>
        <script type="text/javascript" >
            var JSON_PATH = "{{ route('clients.json',1 ) }}";
            var JSON_RESPONSE_PATH = "{{ route('clients.json.response',1 ) }}";
            var TOKEN = "{{ csrf_token() }}";
        </script>
    </body>
</html>