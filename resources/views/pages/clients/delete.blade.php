<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <style>
            td{
                 border:1px solid #333;
                 padding:10px;
            }            
        </style>
    </head>
    <body>

        <form action="{{route('clients.destroy', $model['id'])}}" method="post">
            <input style='color:red;' type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="_method" value="delete">
            <p>
                <label>Are you sure to deleting this item:</label>
                {{ $model['name'] }}              
            </p>

            <input type="submit" value="Destroy">
        </form>
        
    </body>
</html>