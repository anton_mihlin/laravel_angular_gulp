<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <style>
            td{
                 border:1px solid #333;
                 padding:10px;
            }            
        </style>
    </head>
    <body>

        <form action="{{route('clients.update', $model['id'])}}" method="post">
            <input style='color:red;' type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="_method" value="put">
            <p>
                <label>Name</label>
                <input type="text" name="name" value="{{ $model['name'] }}" >                
            </p>
            <p>
                <label>User</label>
                <select name="client">
                    @foreach( $clients as $client)
                    <option 
                        @if($client['id'] === $model['client_id'] )
                            selected="selected"
                        @endif
                        value="{{ $client['id'] }}">
                        {{ $client['name'] }}
                    </option>
                    @endforeach
                </select>
            </p>
            <input type="submit" value="Update">
        </form>
        
    </body>
</html>