@if (count($errors) > 0)
<div id="errors">      
    @foreach($errors->all() as $error)
    {{ $error }}              
    @endforeach        
</div>
@endif