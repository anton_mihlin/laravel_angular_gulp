<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <style>
            td{
                 border:1px solid #333;
                 padding:10px;
            }            
        </style>
    </head>
    <body>

        @if (Session::has('myMessage'))
        <h2>{{ Session::get('myMessage') }}</h2>
        @endif
        
        <table>
            <tr>
                <td>Id</td>
                <td>Name</td>
                <td>User id</td>
            </tr>
            @foreach( $models as $item)
            <tr>
                <td>{{$item['id']}}</td>
                <td>{{$item['name']}}</td>
                <td>{{$item['user_id']}}</td>
                <td>{{ $item['full_name'] }}</td>
                <td>
                    <a href="{{ route('treeApp.edit', $item['id']) }}">Edit</a>
                    <a href="{{ route('treeApp.delete', $item['id']) }}">Delete</a>
                </td>
            </tr>
            @endforeach            
        </table>

    </body>
</html>