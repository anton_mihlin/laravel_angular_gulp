@extends('example.firstView')

@section('content')
	<p>This is our extended template</p>
	<h1>{{ trans('titles.firstTitle') }}</h1>
	
	@if (Session::has('myMessage'))
		<p>{{ Session::get('myMessage') }}</p>
	@endif
	
	<form method="post" action="{{ route('my.example.post') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="submit" value="Send to post">
	</form>
@endsection