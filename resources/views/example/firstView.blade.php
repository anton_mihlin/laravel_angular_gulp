<!DOCTYPE html>
<html>
    <head>
        <title>My first view</title>
	</head>
    <body>
        <h1>{{$stringOne}}</h1>
        @foreach( $arrayOne as $item)
            <p>{{$item}}</p>
        @endforeach

        @include('example.partials.includeView')

        @yield('content')
    </body>
</html>
