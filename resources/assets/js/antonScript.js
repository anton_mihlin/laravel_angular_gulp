/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var SampleClass = function(){
    var that = this;
    
    that.init = function(){
        //that.alertingMethod();
        that.getAjaxColor();
    };
    
    that.alertingMethod = function(){
        alert('This is Sample method from SampleClass.');
        
    };
    
    that.getAjaxColor = function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': TOKEN
            }
        });
        $.ajax({
            url: JSON_RESPONSE_PATH

        }).done(function (response) {
            if (response.status = 'success') {
                //alert(response.text);
                $('.change-color').css('backgroundColor', response);
            }
        });
    };
    
};

var SAMPLE_CLASS = null;

$(function(){
    var SAMPLE_CLASS = new SampleClass;
    //SAMPLE_CLASS.init();
   
    $('.change-color').click(function(){
        SAMPLE_CLASS.getAjaxColor();

    });
    
});

