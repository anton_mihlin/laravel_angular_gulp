<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Helpers;
/**
 * Description of TextStorage
 *
 * @author Anton
 */
class CleanStringHelper {
    public function removeSpecialChar( $string ){
        $newString = preg_replace('/[^a-z0-9]/i','_', $string);
        return $newString;
    }
}
