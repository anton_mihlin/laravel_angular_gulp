<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    public function user(){        
        return $this->belongsTo('App\Models\User');
    }
    /*
     * Custom data
     */
    public function getFullNameAttribute(){
        return 'Task with name'. $this->name;
    }
}
