<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function client(){        
        return $this->belongsTo('App\Models\Client');
    }
    /*
     * Custom data
     */
    public function getFullNameAttribute(){
        return 'Product with name: '. $this->name;
    }
}
