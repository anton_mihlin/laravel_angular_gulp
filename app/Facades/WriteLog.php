<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class WriteLog extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'writeLog';
    }
}