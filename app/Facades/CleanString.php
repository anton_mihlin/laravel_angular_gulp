<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class CleanString extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'cleanString';
    }
}