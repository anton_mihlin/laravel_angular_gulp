<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class ToNumber extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'toNumber';
    }
}