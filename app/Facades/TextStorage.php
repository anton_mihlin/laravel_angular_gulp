<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class TextStorage extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'textStorage';
    }
}