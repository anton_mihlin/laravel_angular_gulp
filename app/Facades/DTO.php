<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class DTO extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'dto';
    }
}