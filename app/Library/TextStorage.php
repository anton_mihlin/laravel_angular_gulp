<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Library;
/**
 * Description of TextStorage
 *
 * @author Anton
 */
class TextStorage {
    public function store(){
        $filePath = storage_path() . config('storage.basePath') . config('storage.textPath');
        
        $myfile = fopen( $filePath . "example_text.txt", "w") or die("Unable to open file!");
        
        $txt = "Mickey Mouse\n";
        fwrite($myfile,$txt);
        
        fclose($myfile);
    }
}
