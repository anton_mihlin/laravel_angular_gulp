<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Library;

/**
 * Description of LogsStorage
 *
 * @author Anton
 */
class LogsStorageLibrary {
    public function write( $item ){
        
        $filePath = storage_path() . config('storage.basePath') . config('storage.logsPath');
        
        $file = fopen($filePath . 'logs.txt' , "a") or die( "Can't open a file");
        
        $newLogContent = $item . PHP_EOL ;
        
        fwrite( $file, $newLogContent );
        
        fclose($file);
        
    }
}
