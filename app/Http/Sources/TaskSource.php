<?php

namespace App\Http\Sources;

use App\Models\User;
use App\Models\Task;

use App\Facades\DTO;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TaskSource
 *
 * @author Anton
 */
class TaskSource {
    public function edit($id){
        
        $models = User::all();        
        //$models->toArray();
        $models = DTO::get($models);
        
        $model = Task::findOrFail($id);
        $model = DTO::get($model);
        
        //die( json_encode($model));        
        
        $data = [
            'users' => $models,
            'model' =>  $model
        ];
        
        return $data;    
    }
}
