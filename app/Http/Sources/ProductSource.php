<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Sources;

use App\Models\Client;
use App\Models\Product;
use App\Facades\DTO;

/**
 * Description of ProductSource
 *
 * @author Anton
 */
class ProductSource {

    public function index() {
        $models = Product::all();
        $models->toArray();

        $data = [
            'models' => $models
        ];
        return $data;
    }

    public function show($id) {
        $model = Product::with('client')->find($id);
        $model->toArray();

        $data = [
            'model' => $model
        ];

        return $data;
    }

    public function create() {

        $models = Client::all();

        $models->toArray();

        $data = [
            'clients' => $models
        ];

        return $data;
    }

    public function edit($id) {

        $models = Client::all();
        //$models->toArray();
        $models = DTO::get($models);

        $model = Product::findOrFail($id);
        //$model->toArray();
        //$model = DTO::get($model,['name']);
        $model = DTO::get($model);

        $data = [
            'clients' => $models,
            'model' => $model
        ];

        return $data;
    }

    public function delete($id) {

        $model = Product::findOrFail($id);
        $model->toArray();

        $data = [
            'model' => $model
        ];

        return $data;
    }

}
