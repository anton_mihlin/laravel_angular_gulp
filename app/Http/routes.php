<?php

/*
  |--------------------------------------------------------------------------
  | Routes File
  |--------------------------------------------------------------------------
  |
  | Here is where you will register all of the routes in an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

Route::get('/', function () {
    return view('welcome');
});

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | This route group applies the "web" middleware group to every route
  | it contains. The "web" middleware group is defined in your HTTP
  | kernel and includes session state, CSRF protection, and more.
  |
 */

Route::group(['middleware' => ['web']], function () {
    Route::get('/some/path', [
        'as' => 'my.example.get',
        'uses' => 'FirstController@getMethod'
    ]);
    Route::post('/some/post', [
        'as' => 'my.example.post',
        'uses' => 'FirstController@postMethod'
    ]);
    
   // Delete
    Route::get('/treeApp/{id}/delete',[
        'as' => 'treeApp.delete',
        'uses' => 'SecondController@delete'
    ]);
    
    Route::resource('treeApp', 'SecondController');
    
    // Language route
    Route::post('/treeApp/lang/example',[
        'as' => 'treeApp.lang',
        'uses' => 'SecondController@lang'
    ]);
    
    // Resource controller routes
    // 
   // Delete
    Route::get('/clients/{id}/delete',[
        'as' => 'clients.delete',
        'uses' => 'ResourceCtrl@delete'
    ]);
    
    Route::resource('clients', 'ResourceCtrl');
    
    Route::get('/clients/{id}/json',[
        'as' => 'clients.json',
        'uses' => 'ResourceCtrl@json'
    ]);
    
    Route::get('/clients/{id}/json-response',[
        'as' => 'clients.json.response',
        'uses' => 'ResourceCtrl@jsonResponse'
    ]);
});
