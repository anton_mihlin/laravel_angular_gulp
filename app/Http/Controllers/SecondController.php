<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Config;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;

use App\Models\Task;
use App\Models\User;

use App\Http\Requests\TaskStoreRequest;

use App\Http\Sources\TaskSource;


class SecondController extends Controller {
    protected $source;
    
    function __construct(){
        $this->source = new TaskSource;    
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $models = Task::all();
        
        $models->toArray();
        
        
        $data = [
            'models' => $models
        ];
        return view('pages/mainView', $data);
    }
    
    /**/public function lang(){
        //Session::set('lang', $lang );
        App::setLocale('it');
        //die('canceled');
        return redirect(route('treeApp.index'));
        //return Redirect::back();
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $models = User::all();
        
        $models->toArray();
        
        
        $data = [
            'users' => $models
        ];
        return view('pages/create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TaskStoreRequest $request) {
        $fields = $request->all();
        
        $userModel = User::findOrFail( $fields['user'] );
        
        $model = new Task;
        $model->name = $fields['name'];
        $model->user()->associate($userModel);
        $model->save();
        
        session()->flash('myMessage','Task is stored');
        return redirect(route('treeApp.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //$model = Task::find($id);
        $model = Task::with('user')->find($id);
        $model->toArray();
        
                
        $data = [
            'model' => $model
        ];
        return view('pages/show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        
        $data = $this->source->edit($id);

        return view('pages/edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $fields = $request->all();
        
        $userModel = User::findOrFail( $fields['user'] );
        
        $model =  Task::findOrFail($id);
        $model->name = $fields['name'];
        $model->user()->associate($userModel);
        $model->save();
        
        session()->flash('myMessage','Task is updated');
        return redirect(route('treeApp.index'));
    }

    /**
     * Show the form for deleting the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id) {
        
        $model = Task::findOrFail($id);
        $model->toArray();
                
        $data = [
            'model' =>  $model
        ];
        return view('pages/delete', $data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        
        $model =  Task::findOrFail($id);
        $model->delete();
        
        session()->flash('myMessage','Task is deleted');
        return redirect(route('treeApp.index'));
    }
}
