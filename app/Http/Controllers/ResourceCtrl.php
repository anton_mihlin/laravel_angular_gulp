<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Product;
use App\Models\Client;

use App\Http\Requests\ClientStoreRequest;

use App\Http\Sources\ProductSource;

use App\Facades\TextStorage;
use App\Facades\CleanString;

use App\Facades\ToNumber;
use App\Facades\WriteLog;

//use App\Library\TextStorage;
//use App\Helpers\CleanStringHelper;

use App\Library\LogsStorageLibrary;
use App\Helpers\ToNumberHelper;

class ResourceCtrl extends Controller {
    protected $source;
    
    function __construct(){
        $this->source = new ProductSource;    
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data = $this->source->index();

        return view('pages/clients/index', $data);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data = $this->source->show();

        //$model = Task::find($id);

        return view('pages/clients/show', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {        
        
        $data = $this->source->create();
        
        return view('pages/clients/create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClientStoreRequest $request) {
        
        $fields = $request->all();

        $clientModel = Client::findOrFail($fields['client']);
        
        //*To use a helper directly
        //$toNumberHelper = new ToNumberHelper;
        
        //*To use a helper through provider
        $toNumberHelper = App::make('toNumber');
        
        $model = new Product;
        
        //*Convert a field through a Helper dice or Provider
        //$model->name = $toNumberHelper->convert( $fields['name'] );  
         
        //*Convert a field through Facade
        $model->name = ToNumber::convert( $fields['name'] );        
        
        $model->client()->associate($clientModel);
        
        //*To use a Library directly
        //$logsStorageLibrary = new LogsStorageLibrary;
        
        //*To use a Library through Provider 
        //$logsStorageLibrary = App::make('writeLog');
        //$logsStorageLibrary->write( date('l \t\h\e jS G:i:s').' - Product ' . $fields['name'] . ' has been created ' );
        
        //*To use a Library through a Facade
        WriteLog::write( date('l \t\h\e jS G:i:s').' - Product ' . $fields['name'] . ' has been created ' );
        
        $model->save();

        session()->flash('myMessage', 'Product is stored');
        return redirect(route('clients.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        $data = $this->source->edit($id);
        
        return view('pages/clients/edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $fields = $request->all();

        $clientModel = Client::findOrFail($fields['client']);

        $model = Product::findOrFail($id);
        $model->name = $fields['name'];
        $model->client()->associate($clientModel);
        $model->save();

        session()->flash('myMessage', 'Product is updated');
        return redirect(route('clients.index'));
    }

    /**
     * Show the form for deleting the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id) {

        $data = $this->source->delete($id);
        
        return view('pages/clients/delete', $data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {

        $model = Product::findOrFail($id);
        $model->delete();

        session()->flash('myMessage', 'Product is deleted');
        return redirect(route('clients.index'));
    }
    
    /**
     * Example of json response
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function json($id) {
        $data = [
            'status' =>'Success',
            'text'=>'Operation complete'            
        ];
        
        return response()->json($data);
    }
    
    public function jsonResponse($id) {
        $data = [
            '#bf7777',
            '#bf77af',
            '#097380',
            '#9277bf',
        ];
        
        $rand_key = array_rand($data);
        
        return response()->json( $data[$rand_key] );
    }
}