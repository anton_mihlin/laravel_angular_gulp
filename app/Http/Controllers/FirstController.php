<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class FirstController extends Controller {
    /*
     *
     *
     * @return Response
     */

    public function getMethod() {
        //return (new Response('some text',200));
        //return response('some text',200);

        $data = [
            'stringOne' => 'My first string',
            'arrayOne' => [
                1,
                2,
                3
            ]
        ];

        return view('example/secondView', $data);
    }

    public function postMethod() {
        session()->flash('myMessage', 'This is my first message');
        return redirect(route('my.example.get'));
    }
}