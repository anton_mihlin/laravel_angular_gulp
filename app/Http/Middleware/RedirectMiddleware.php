<?php

namespace App\Http\Middleware;

use Closure;

class RedirectMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $redirect = $request->input('redirect-checkbox');
        
        if( filter_var( $redirect , FILTER_VALIDATE_BOOLEAN) ){
            return redirect('http://google.it');
        }
        
        return $next($request);
    }
}
