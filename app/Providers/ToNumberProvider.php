<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Helpers\ToNumberHelper;

class ToNumberProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('toNumber', function() {
            return new ToNumberHelper;
        });
    }
}
