<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\DTOs\DTO;

class DTOServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('dto', function() {
            return new DTO;
        });
    }
}
