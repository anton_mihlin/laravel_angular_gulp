<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Helpers\CleanStringHelper;

class CleanStringProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('cleanString', function() {
            return new CleanStringHelper;
        });
        
        /*
         * If you have many helpers just add same row with helpers' names
         * 
            $this->app->singleton('cleanString', function() {
                return new CleanStringHelper;
            });
         * 
         * 
         */
        
    }
}
