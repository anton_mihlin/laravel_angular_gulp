<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Library\TextStorage;

class TextStorageProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('textStorage', function() {
            return new TextStorage;
        });
    }
}
