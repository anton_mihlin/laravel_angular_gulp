<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Library\LogsStorageLibrary;

class LogsStorageProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('writeLog', function() {
            return new LogsStorageLibrary;
        });
    }
}
