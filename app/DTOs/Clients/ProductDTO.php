<?php

namespace App\DTOs\Clients;

use App\DTOs\DTOEngine;

class ProductDTO extends DTOEngine
{
    public function data($model, $keys)
    {
        if (!$keys) $keys = [
            'id',
            'name',
            'client_id',
            'client.name',
        ];
        $data = $this->get($keys, $model);
        
        //$data['question'] = 'What do you think about this client?';
        
        //die( json_encode( $data ) );

        return $data;
    }
}
