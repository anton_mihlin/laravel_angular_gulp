<?php

namespace App\DTOs\Clients;

use App\DTOs\DTOEngine;

class ClientDTO extends DTOEngine
{
    public function data($model, $keys)
    {
        if (!$keys) $keys = [
            'id',
            'name',
            'email',
        ];
        $data = $this->get($keys, $model);

        return $data;
    }
}
