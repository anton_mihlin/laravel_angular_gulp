<?php

namespace App\DTOs;

use App\Facades\Format;

class DTOEngine
{
    public function get($keys, $model, $children = [])
    {
        $data = [];

        foreach ($keys as $key) {
            // Filters
            $filters = explode('|', $key);
            $key = $filters[0];
            unset($filters[0]);

            // Name
            $nodes = explode('.', $key);
            $length = count($nodes);
            $name = $nodes[0];
            if ($length > 1) $name = strtolower($nodes[$length - 2]) . ucfirst($nodes[$length - 1]);

            // Value
            $data[$name] = $this->value($key, $model, $children, $filters);
        }

        return $data;
    }

    public function value($name, $model, $children = [], $filters = [])
    {
        $value = null;
        $chain = explode('.', $name);
        $level = $model;

        foreach ($chain as $i => $field) {
            if (is_callable([$level, $field])) {

                if ($this->isBin($level->$field)) {
                    if ($i === count($chain) - 1 && isset($children[$field])) {
                        // Array
                        $data = [];
                        foreach ($level->$field as $item) {
                            $data[] = $this->get($children[$field], $item, $children);
                        }
                        $value = $data;
                    } else {
                        // Indirect array field
                        $value = $level->$field;
                        $level = (count($value)) ? $value[0] : null;
                    }
                } else {
                    // Field & indirect field
                    $value = $this->filter($level->$field, $filters);
                    $level = $level->$field;
                }

            }
        }

        return $value;
    }

    public function filter($value, $filters)
    {
        $result = $value;

        if (gettype($result) === 'string' ||
            (gettype($result) === 'object' && get_class($result) === 'Carbon\Carbon')) {
            foreach ($filters as $filter) {
                $options = explode(':', $filter, 2);

                switch ($options[0]) {
                    case "int":
                        $result = intval($result);
                        break;
                    case "float":
                        $result = floatval($result);
                        break;
                    case "date":
                        $format = isset($options[1]) ? $options[1] : 'd/m/Y';
                        $result = Format::parse($result, $format);
                        break;
                    case "datetime":
                        $result = Format::parse($result, 'd/m/Y H:i:s');
                        break;
                }
            }
        }

        return $result;
    }

    public function isBin($item)
    {
        $result = false;

        if (gettype($item) === 'object') {
            if (strpos(get_class($item), 'Collection') !== false) $result = true;
        }

        return $result;
    }

    public function isModel($item)
    {
        return strpos(get_class($item), 'Models') !== false;
    }
}
