<?php

namespace App\DTOs;

use Illuminate\Support\Facades\App;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

class DTO
{
    public function get($model, $keys = null)
    {
        $data = [];

        if ($model && (!$this->isBin($model) || count($model))) {
            $class = $this->getName($model);

            if ($class) {
                $object = new $class;

                if ($this->isBin($model)) {
                    foreach ($model as $element) {
                        $data[] = $object->data($element, $keys);
                    }
                } else if ($this->isModel($model)) {
                    $data = $object->data($model, $keys);
                }
            } else {
                App::abort(500, 'Model not found in the DTO.');
            }
        }

        return $data;
    }

    public function select($model, $labelField = 'name', $emptyOption = true, $emptyMessage = null)
    {
        $selectData = [];

        $data = $this->get($model);
        if (gettype($data) != 'array') $data = [$data];

        if ($emptyOption) {
            if (!$emptyMessage) $emptyMessage = trans('messages.selectItem');
            $selectData[''] = $emptyMessage;
        }

        foreach ($data as $element) {
            $selectData[$element['id']] = $element[$labelField];
        }

        return $selectData;
    }

    public function map($model, $keyField = 'name')
    {
        $mapData = [];

        $data = $this->get($model);

        foreach ($data as $element) {
            $mapData[$element[$keyField]] = $element['id'];
        }

        return $mapData;
    }

    public function isBin($model)
    {
        $result = false;

        if (gettype($model) === 'object') {
            $className = get_class($model);
            if (strpos($className, 'Paginator') !== false || strpos($className, 'Collection') !== false) {
                $result = true;
            }
        }

        return $result;
    }

    public function isModel($model)
    {
        return strpos(get_class($model), 'Models') !== false;
    }

    public function getName($model)
    {
        if ($this->isBin($model)) $model = $model[0];
        $result = null;

        $class = get_class($model);
        $class = explode('\\', $class);
        $class = $class[count($class) - 1];

        $search = $class . 'DTO';

        // Search DTO class
        $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(__DIR__));
        foreach ($iterator as $fileInfo) {
            if ($fileInfo->getFilename() === $search . '.php') {
                $ns = str_replace(__DIR__, '', $fileInfo->getPath());
                $ns = str_replace('/', '\\', $ns);
                $result = __NAMESPACE__ . $ns . '\\' . $search;
            }
        }

        return $result;
    }
}
