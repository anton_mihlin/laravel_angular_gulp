<?php

namespace App\DTOs\Example;

use App\DTOs\DTOEngine;

class UserDTO extends DTOEngine
{
    public function data($model, $keys)
    {
        if (!$keys) $keys = [
            'id',
            'name',
            'email',
        ];
        $data = $this->get($keys, $model);

        return $data;
    }
}
