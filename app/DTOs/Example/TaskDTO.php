<?php

namespace App\DTOs\Example;

use App\DTOs\DTOEngine;

class TaskDTO extends DTOEngine
{
    public function data($model, $keys)
    {
        if (!$keys) $keys = [
            'id',
            'name',
            'user_id',
            'user.name',
        ];
        $data = $this->get($keys, $model);
        
        $data['newThing'] = 'This is a new data';

        return $data;
    }
}
